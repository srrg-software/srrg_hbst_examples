#include <dirent.h>
#include <iostream>
#include <srrg_hbst/types/binary_tree.hpp>

using namespace srrg_hbst;
typedef BinaryTree256<size_t> Tree;

int main(int argc_, char** argv_) {
  // ds validate input
  if (argc_ != 3) {
    std::cerr
      << "invalid call - please use: ./reader /path/to/input/database.hbst /path/to/image_folder"
      << std::endl;
    return 0;
  }

  // ds initialize feature handling
#if CV_MAJOR_VERSION == 2
  cv::Ptr<cv::FeatureDetector> keypoint_detector        = new cv::FastFeatureDetector();
  cv::Ptr<cv::DescriptorExtractor> descriptor_extractor = new cv::ORB();
#elif CV_MAJOR_VERSION == 3
  cv::Ptr<cv::FeatureDetector> keypoint_detector        = cv::FastFeatureDetector::create();
  cv::Ptr<cv::DescriptorExtractor> descriptor_extractor = cv::ORB::create();
#endif

  // ds get paths
  const std::string file_path_database = argv_[1];
  const std::string folder_images      = argv_[2];
  std::cerr << "loading database from: " << file_path_database << " .." << std::endl;

  // ds load database
  Tree database;
  if (database.read(file_path_database)) {
    std::cerr << "successfully loaded database" << std::endl;
  } else {
    std::cerr << "ERROR: unable to load database: " << file_path_database << std::endl;
    database.clear(true);
    return 0;
  }

  // ds parse available images in the provided folder
  std::cerr << "  loading images from: " << folder_images << std::endl;
  std::vector<std::string> file_paths_images;
  std::string image_encoding;
  DIR* directory_handle = opendir(folder_images.c_str());
  struct dirent* entry;
  while ((entry = readdir(directory_handle)) != NULL) {
    if (entry->d_type == char(8) /*file*/) {
      std::string file_name   = entry->d_name;
      const size_t index_mime = file_name.find_first_not_of("0123456789");
      if (index_mime == 0 || index_mime == std::string::npos) {
        std::cerr << "ERROR: image names must start with a number!" << std::endl;
        return 0;
      }
      if (image_encoding.empty()) {
        image_encoding = file_name.substr(index_mime);
      }
      file_paths_images.push_back(file_name.substr(0, index_mime));
    }
  }
  closedir(directory_handle);
  std::cerr << "  found image paths: " << file_paths_images.size() << std::endl;
  std::cerr << "press [ENTER] to start processing" << std::endl;
  getchar();

  // ds sort images by identifier
  std::sort(
    file_paths_images.begin(),
    file_paths_images.end(),
    [](const std::string& a_, const std::string& b_) { return std::stod(a_) < std::stod(b_); });

  size_t number_of_processed_images = 0;
  for (const std::string& file_path_image : file_paths_images) {
    const std::string full_file_path_image = folder_images + "/" + file_path_image + image_encoding;
    // ds load image (project root folder)
    const cv::Mat image(cv::imread(full_file_path_image, CV_LOAD_IMAGE_GRAYSCALE));
    if (image.rows < 1 || image.cols < 1) {
      std::cerr << "ERROR: unable to load image: " << full_file_path_image << std::endl;
      return 0;
    }

    // ds detect FAST keypoints
    std::vector<cv::KeyPoint> keypoints;
    keypoint_detector->detect(image, keypoints);

    // ds compute BRIEF descriptors
    cv::Mat descriptors;
    descriptor_extractor->compute(image, keypoints, descriptors);
    std::cerr << "processed image: " << full_file_path_image << " features: " << descriptors.rows
              << std::endl;

    // ds convert descriptors to matchables and query the tree
    std::vector<size_t> indices(descriptors.rows, 0);
    const Tree::MatchableVector matchables_query(std::move(
      Tree::getMatchables(descriptors, indices, database.size() + number_of_processed_images)));
    Tree::MatchVectorMap matches_per_image;
    database.match(matchables_query, matches_per_image, 30);
    for (const Tree::MatchVectorMapElement& matches : matches_per_image) {
      const double matching_ratio =
        static_cast<double>(matches.second.size()) / matchables_query.size();
      if (matching_ratio > 0.1) {
        std::cerr << " - matches: " << matches.first << " (" << matches.second.size() << ":"
                  << matching_ratio << ")" << std::endl;
      }
    }
    ++number_of_processed_images;

    // ds visuals
    cv::Mat image_display;
    cv::cvtColor(image, image_display, CV_GRAY2BGR);
    for (const cv::KeyPoint& keypoint : keypoints) {
      cv::circle(image_display, keypoint.pt, 1, cv::Scalar(255, 0, 0), -1);
    }
    cv::imshow("current image", image_display);
    cv::waitKey(1);
  }
  std::cerr << "processed images: " << number_of_processed_images << std::endl;

  // ds clear database
  database.clear(true);
  return 0;
}
